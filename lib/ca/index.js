const compiler = require('@nx-js/compiler-util')
import { EventEmitter } from 'events'
import { ForthVM, Word } from '../forth'

const DIRECTION_TABLE = {
  nw: 0,
  n: 1,
  ne: 2,
  w: 3,
  e: 4,
  sw: 5,
  s: 6,
  se: 7
}

class CAForthVM extends ForthVM {
  constructor() {
    super()
    this.cellInfo = {
      neighbors: [0, 0, 0, 0, 0, 0, 0, 1],
      self: 0
    }
    this.dictionary['c'] = new Word({
      name: 'c',
      code: () => {
        this.dataStack.push(this.cellInfo.self)
      }
    })
    for (let dir in DIRECTION_TABLE) {
      this.dictionary[dir] = new Word({
        name: dir,
        code: () => {
          this.dataStack.push(this.cellInfo.neighbors[DIRECTION_TABLE[dir]])
        }
      })
    }
    this.dictionary['neighbors'] = new Word({
      name: 'neighbors',
      code: () => {
        this.cellInfo.neighbors.forEach(n => {
          this.dataStack.push(n)
        })
      }
    })
  }
  updateCellInfo({ self, neighbors }) {
    this.cellInfo.self = self
    this.cellInfo.neighbors = neighbors
  }
  make__Rule(program) {
    program = `: rule ${program} ;`
    this.parse(program)
    this.run()
    let f = this.dictionary['rule'].code
    let ruleTable = new Array(512).fill(0)
    for (let i = 0; i < 512; i++) {
      this.dataStack.clear()
      let cells = i
        .toString(2)
        .padStart(9, '0')
        .split('')
        .map(v => parseInt(v))
      let self = cells.splice(4, 1)[0]
      this.updateCellInfo({ self: self, neighbors: cells })
      f()
      ruleTable[i] = this.dataStack.tos()
    }
    return {
      program: program,
      fun: neighbors => {
        let i = neighbors.reverse().reduce((a, v, i) => {
          return Math.pow(2, i) * v + a
        }, 0)
        return ruleTable[i]
      },
      table: ruleTable
    }
  }

  makeRule(program) {
    program = `: rule ${program} ;`
    this.parse(program)
    this.run()
    let f = this.dictionary['rule'].code
    return {
      program: program,
      fun: (self, neighbors) => {
        this.dataStack.clear()
        this.updateCellInfo({ self: self, neighbors: neighbors })
        f()
        return this.dataStack.tos()
      }
    }
  }
}

export class TwoDimCellularAutomaton extends EventEmitter {
  // K = 2, r = 1
  constructor({ width = 600, height = 300, stateNumber = 2 }) {
    super()
    this.width = width
    this.height = height
    this.lattice = new Array(height).fill(0).map(() => new Array(width).fill(0))
    this.nextl = new Array(height).fill(0).map(() => new Array(width).fill(0))
    this.age = 0
    this.forth = new CAForthVM()
    this.rule = this.forth.makeRule('c')
    this.stateNumber = stateNumber
  }
  initWorld({ density = 0.5 }) {
    this.age = 0
    this.lattice.forEach(row => {
      row.forEach((v, i) => {
        row[i] = Math.floor(Math.random() * this.stateNumber)
      })
    })
    this.updated()
    return this.lattice
  }
  oneStep() {
    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        // this.nextl[y][x] = this.rule.fun(this.neighborsAt(x, y))
        this.nextl[y][x] = this.rule.fun(
          this.lattice[y][x],
          this.neighborsAt(x, y)
        )
      }
    }
    let tmp = this.lattice
    this.lattice = this.nextl
    this.nextl = tmp
    this.age += 1
    this.updated()
    return this.lattice
  }

  // Moore neighbors including self.
  neighborsAt(x, y) {
    let py = y === 0 ? this.height - 1 : y - 1
    let ny = y === this.height - 1 ? 0 : y + 1
    let px = x === 0 ? this.width - 1 : x - 1
    let nx = x === this.width - 1 ? 0 : x + 1
    // prettier-ignore
    return [
      this.lattice[py][px],this.lattice[py][ x],this.lattice[py][nx],
      this.lattice[ y][px],                     this.lattice[ y][nx],
      this.lattice[ny][px],this.lattice[ny][ x],this.lattice[ny][nx]
    ]
  }
  updated() {
    this.emit('updated', {
      lattice: this.lattice,
      age: this.age
    })
  }
  jsMakeRule(body) {
    // prettier-ignore
    body = `
"use strict";
const nw = neighbors[${DIRECTION_TABLE.nw}],
       n = neighbors[${DIRECTION_TABLE.n}],
      ne = neighbors[${DIRECTION_TABLE.ne}],
       w = neighbors[${DIRECTION_TABLE.w}],
       e = neighbors[${DIRECTION_TABLE.e}],
      sw = neighbors[${DIRECTION_TABLE.sw}],
       s = neighbors[${DIRECTION_TABLE.s}],
      se = neighbors[${DIRECTION_TABLE.se}],
      currentState = c;
let nextState = 0;
const stateNumber = ${this.stateNumber};
${body};
return nextState;
`
    const code = compiler.compileCode(body)

    let fun = function(currentState, neighbors) {
      return code({
        c: currentState,
        neighbors,
        Math
      })
    }
    return {
      program: body,
      fun
    }
  }
  setRule({ language, body }) {
    let rule = null
    try {
      switch (language) {
        case 'forth':
          rule = this.forth.makeRule(body)
          break
        case 'javascript':
          rule = this.jsMakeRule(body)
        default:
          break
      }
    } catch (e) {
      throw e
      return
    }
    this.rule = rule
  }
  setStateNumber(stateNumber) {
    this.stateNumber = stateNumber
  }
}

export class OneDimCellularAutomaton extends TwoDimCellularAutomaton {
  constructor({ width = 600, stateNumber = 2 }) {
    super()
    this.width = width
    this.lattice = new Array(width).fill(0)
    this.nextl = new Array(width).fill(0)
    this.age = 0
  }
  initWorld({ isRandom } = {}) {
    this.lattice.fill(0)
    this.age = 0
    if (isRandom) {
      this.lattice.forEach((v, i) => {
        this.lattice[i] = Math.floor(Math.random() * this.stateNumber)
      })
    } else {
      let half = Math.floor(this.width / 2)
      this.lattice[half] = 1
    }
    this.updated()
    return this.lattice
  }
  setRule({ language, body }) {
    let rule = null
    try {
      switch (language) {
        case 'javascript':
          rule = this.jsMakeRule(body)
          break
        default:
          throw new Error('unsupported language')
          break
      }
    } catch (e) {
      throw e
      return
    }
    this.rule = rule
  }
  jsMakeRule(body) {
    // prettier-ignore
    body = `
"use strict";
const w = neighbors[0],
      e = neighbors[1],
      currentState = c;
let nextState = 0;
const stateNumber = ${this.stateNumber};
${body};
return nextState;
`
    const code = compiler.compileCode(body)
    let fun = function(currentState, neighbors) {
      return code({
        c: currentState,
        neighbors,
        Math
      })
    }
    return {
      program: body,
      fun
    }
  }
  neighborsAt(x) {
    let px = x === 0 ? this.width - 1 : x - 1
    let nx = x === this.width - 1 ? 0 : x + 1
    return [this.lattice[px], this.lattice[nx]]
  }
  oneStep() {
    this.lattice.forEach((v, i) => {
      let self = this.lattice[i]
      this.nextl[i] = this.rule.fun(self, this.neighborsAt(i))
    })
    let tmp = this.lattice
    this.lattice = this.nextl
    this.nextl = tmp
    this.age += 1
    this.updated()
    return this.lattice
  }
}
