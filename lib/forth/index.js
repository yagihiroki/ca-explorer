function nop() {}

class Stack {
  constructor(n = 128) {
    this.data = new Array(n).fill(0)
    this.sp = -1
  }

  pop() {
    if (this.sp === -1) {
      throw new Error('stack underflow')
    }
    let n = this.data[this.sp]
    this.sp -= 1
    return n
  }

  push(n) {
    this.sp += 1
    this.data[this.sp] = n
    return n
  }

  tos() {
    if (this.sp === -1) {
      throw new Error('stack underflow')
    }
    return this.data[this.sp]
  }

  swap() {
    // x1 x2 -- x2 x1
    let x2 = this.pop()
    let x1 = this.pop()
    this.push(x2)
    this.push(x1)
  }

  clear() {
    this.sp = -1
    this.data.fill(0)
    return this
  }

  sum() {
    return this.data.slice(0, this.sp + 1).reduce((v, a) => v + a, 0)
  }

  toArray() {
    return this.data.slice(0, this.sp + 1)
  }

  toString() {
    let s = ''
    for (let i = 0; i <= this.sp; i++) {
      s += this.data[i] + ' '
    }
    return s
  }
}

export class Word {
  constructor({ name, code, parameter = [], isImmediate, compileOnly }) {
    this.name = name
    this.code = code
    this.parameter = parameter
    this.isImmediate = isImmediate
    this.compileOnly = compileOnly
  }
}

const INTERPRET_STATE = 0
const COMPILE_STATE = 1

export class ForthVM {
  static isTrue(n) {
    return n != 0
  }

  constructor() {
    this.dataStack = new Stack()
    this.returnStack = new Stack()
    this.textBuffer = ''
    this.program = []
    this.programCounter = 0
    this.state = INTERPRET_STATE
    this.currentWord = null
    this.dictionary = {
      clear: new Word({
        name: 'clear',
        code: () => {
          this.dataStack.clear()
        }
      }),
      // x - x x
      dup: new Word({
        name: 'dup',
        code: () => {
          this.dataStack.push(this.dataStack.tos())
        }
      }),
      // x1 x2 -- x2 x1
      swap: new Word({
        name: 'swap',
        code: () => {
          this.dataStack.swap()
        }
      }),
      // x1 x2 -- x1 x2 x1
      over: new Word({
        name: 'over',
        code: () => {
          let x2 = this.dataStack.pop()
          let x1 = this.dataStack.tos()
          this.dataStack.push(x2)
          this.dataStack.push(x1)
        }
      }),
      // n1 n2 -- n3
      '+': new Word({
        name: '+',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          this.dataStack.push(n1 + n2)
        }
      }),
      // n1 n2 -- n3
      '-': new Word({
        name: '-',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          this.dataStack.push(n1 - n2)
        }
      }),
      // n1 n2 -- n3
      '*': new Word({
        name: '*',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          this.dataStack.push(n1 * n2)
        }
      }),
      // n1 n2 -- n3
      '/': new Word({
        name: '/',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          this.dataStack.push(n1 / n2)
        }
      }),
      // n1 n2 -- n3
      mod: new Word({
        name: 'mod',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          this.dataStack.push(n1 % n2)
        }
      }),
      rand: new Word({
        name: 'rand',
        code: () => {
          this.dataStack.push(Math.random())
        }
      }),
      // n1 n2 -- flag
      '=': new Word({
        name: '=',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          if (n1 === n2) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      '!=': new Word({
        name: '!=',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          if (n2 !== n1) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      '<': new Word({
        name: '<',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          if (n1 < n2) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      '>': new Word({
        name: '>',
        code: () => {
          let n2 = this.dataStack.pop()
          let n1 = this.dataStack.pop()
          if (n1 > n2) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      not: new Word({
        name: 'not',
        code: () => {
          let x1 = this.dataStack.pop()
          if (ForthVM.isTrue(x1)) {
            this.dataStack.push(0)
          } else {
            this.dataStack.push(1)
          }
        }
      }),
      and: new Word({
        name: 'and',
        code: () => {
          let x2 = this.dataStack.pop()
          let x1 = this.dataStack.pop()
          if (ForthVM.isTrue(x1) && ForthVM.isTrue(x2)) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      or: new Word({
        name: 'or',
        code: () => {
          // x1 x2 -- x3
          let x2 = this.dataStack.pop()
          let x1 = this.dataStack.pop()
          if (ForthVM.isTrue(x1) || ForthVM.isTrue(x2)) {
            this.dataStack.push(1)
          } else {
            this.dataStack.push(0)
          }
        }
      }),
      // ... n - m
      sum: new Word({
        name: 'sum',
        code: () => {
          let sum = this.dataStack.sum()
          this.dataStack.clear()
          this.dataStack.push(sum)
        }
      }),
      ':': new Word({
        name: ':',
        code: () => {
          this.state = COMPILE_STATE
          this.programCounter++
          let name = this.program[this.programCounter]
          if (!name) {
            throw new Error('no name')
          }
          this.currentWord = new Word({
            name: name
          })
        }
      }),
      ';': new Word({
        name: ';',
        isImmediate: true,
        compileOnly: true,
        code: () => {
          let word = { ...this.currentWord }
          this.currentWord = null
          word.code = () => {
            this.returnStack.push(this.programCounter)
            for (
              this.programCounter = 0;
              this.programCounter < word.parameter.length;
              this.programCounter++
            ) {
              word.parameter[this.programCounter]()
            }
            this.programCounter = this.returnStack.pop()
          }
          this.dictionary[word.name] = word
          this.state = INTERPRET_STATE
        }
      }),
      if: new Word({
        name: 'if',
        isImmediate: true,
        compileOnly: true,
        code: () => {
          let orig = this.currentWord.parameter.length
          this.dataStack.push(orig)
          this.currentWord.parameter.push(destination => {
            if (!destination) {
              throw new Error(`unbalanced 'if-then' structure`)
            }
            this.currentWord.parameter[orig] = () => {
              // x --
              let x = this.dataStack.pop()
              if (!ForthVM.isTrue(x)) {
                this.programCounter = destination
              }
            }
          })
        }
      }),
      else: new Word({
        name: 'else',
        isImmediate: true,
        compileOnly: true,
        code: () => {
          // originate else
          let elseOrig = this.currentWord.parameter.length
          this.dataStack.push(elseOrig)
          this.currentWord.parameter.push(destination => {
            if (!destination) {
              throw new Error(`unbalanced 'if-else-then' structure`)
            }
            this.currentWord.parameter[elseOrig] = () => {
              this.programCounter = destination
            }
          })

          // resolve if
          this.dataStack.swap()
          let ifOrig = this.dataStack.pop()
          let resolver = this.currentWord.parameter[ifOrig]
          if (!resolver) {
            throw new Error(`unbalanced 'if-else-then' structure`)
          }
          resolver(elseOrig)
        }
      }),
      then: new Word({
        name: 'then',
        isImmediate: true,
        compileOnly: true,
        code: () => {
          let orig = this.dataStack.pop()
          let resolver = this.currentWord.parameter[orig]
          if (!resolver) {
            throw new Error(`unbalanced 'if-then' structure`)
          }
          resolver(this.currentWord.parameter.length)
          this.currentWord.parameter.push(nop)
        }
      })
    }
  }

  parse(text) {
    this.textBuffer = text
    this.program = text.toLowerCase().match(/\S+/g) || []
    return this.program
  }

  abort() {
    this.dataStack.clear()
    this.returnStack.clear()
    this.state = INTERPRET_STATE
    this.currentWord = null
  }

  compileNumber(n) {
    this.currentWord.parameter.push(() => {
      this.dataStack.push(n)
    })
  }

  run() {
    for (
      this.programCounter = 0;
      this.programCounter < this.program.length;
      this.programCounter++
    ) {
      let name = this.program[this.programCounter]
      let word = this.dictionary[name]
      if (word) {
        if (this.state === INTERPRET_STATE) {
          if (word.compileOnly) {
            throw new Error(`compile-only word: >>>${name}<<<`)
          }
          word.code()
        } else {
          // compile word
          if (word.isImmediate) {
            word.code()
          } else {
            this.currentWord.parameter.push(word.code)
          }
        }
      } else {
        let num = parseFloat(name)
        if (isNaN(num)) {
          this.abort()
          throw new Error(`undefined word: >>>${name}<<<`)
        }
        if (this.state === INTERPRET_STATE) {
          this.dataStack.push(num)
        } else {
          this.compileNumber(num)
        }
      }
    }
  }
}
