# FORTH

## Call Threading
> Call threading uses indirect calls (which most programming languages have) instead of indirect jumps. For every call, there must also be a return (except for optimized tail-calls, which don't occur here). So the cost of this method is that of using a call-return sequence instead of a simple jump.
> Moreover, each virtual machine instruction is represented by a function (procedure). This is very elegant to look at, and allows separate compilation, but it means that global variables have to be used for the virtual machine registers (e.g., the instruction pointer ip), which most (all?) compilers allocate into memory; in contrast, with switch threading you can use local variables, which are (hopefully) allocated into registers.

参考: http://www.complang.tuwien.ac.at/forth/threaded-code.html
