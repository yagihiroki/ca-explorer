import * as functions from 'firebase-functions';

const db = functions.firestore()

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});

// export const createUser = db
//     .document('rules/{rulesId}')
//     .onWrite(event => {
//         const newTags = event.data.get('tags')
//         const tagsRef = db.collection('tags')
//         return db.runTransaction(transaction => {
//             return transaction.get(tagsRef).then(tagsDoc => {
//                 for (let k in newTags) {
//                     if (newTags[k] && !tagsRef.doc(k).exists) {
//                         tagsRef.doc(k).set({})
//                     }
//                 }                
//             })
//         })
//     })
