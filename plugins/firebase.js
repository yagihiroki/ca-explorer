import firebase from 'firebase/app'
import 'firebase/auth'
// Required for side-effects
require('firebase/firestore')

if (!firebase.apps.length) {
  var config = {
    apiKey: 'AIzaSyBznoA2W2PjbB4rafjL6cHScRa5bT354VU',
    authDomain: 'ca-explorer.firebaseapp.com',
    databaseURL: 'https://ca-explorer.firebaseio.com',
    projectId: 'ca-explorer',
    storageBucket: 'ca-explorer.appspot.com',
    messagingSenderId: '1018060166031'
  }
  firebase.initializeApp(config)
}

export default firebase
