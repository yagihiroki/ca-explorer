import Vuex from 'vuex'
import firebase from '@/plugins/firebase'
import { firebaseMutations, firebaseAction } from 'vuexfire'
const db = firebase.firestore()
const settings = {
  timestampsInSnapshots: true
}
db.settings(settings)
const rulesRef = db.collection('rules')
const provider = new firebase.auth.GoogleAuthProvider()

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      user: null,
      rule: null,
      rules: [],
      isLoaded: false
    }),
    getters: {
      user: state => state.user,
      rules: state => state.rules,
      rule: state => state.rule,
      isLoaded: state => state.isLoaded
    },
    mutations: {
      setCredential(state, { user }) {
        state.user = user
      },
      setIsLoaded(state, next) {
        state.isLoaded = !!next
      },
      setSingleRule(state, { rule }) {
        state.rule = rule
      },
      ...firebaseMutations
    },
    actions: {
      SET_CREDENTIAL({ commit }, { user }) {
        if (!user) return
        commit('setCredential', { user })
      },
      DELETE_CREDENTIAL({ commit }) {
        commit('setCredential', { user: null })
      },
      // prettier-ignore
      INIT_RULES: firebaseAction(async ({ bindFirebaseRef }) => {
        await bindFirebaseRef('rules', rulesRef)
      }),
      // prettier-ignore
      SEARCH_RULES: firebaseAction(async ({ bindFirebaseRef }, { tagsArray }) => {
        await bindFirebaseRef('rules', rulesRef.where(`tags.${tagsArray[0]}`, '==', true))
      }),
      // prettier-ignore
      INIT_SINGLE_RULE: firebaseAction(async ({ commit, bindFirebaseRef, state }, { id }) => {
        await bindFirebaseRef('rule', rulesRef.doc(id))
      }),
      INIT_SINGLE_RULE_TOP({ commit, state }, rule) {
        commit('setSingleRule', { rule })
      },
      // prettier-ignore
      CREATE_RULE(
        { state },
        { title, description, dimension, stateNumber, range, program, tags = {} }) {
        return rulesRef.add({
          title,
          description,
          dimension,
          stateNumber,
          range,
          program,
          author_id: state.user.uid,
          tags
        })
      },
      // prettier-ignore
      UPDATE_RULE(
        ctx,
        { id, title, description, dimension, stateNumber, range, program, tags = {} }) {
        return rulesRef.doc(id).update({
          title,
          description,
          dimension,
          stateNumber,
          range,
          program,
          tags
        })
      },
      // prettier-ignore
      DELETE_RULE(ctx, { id }) {
        return rulesRef.doc(id).delete()
      },
      callAuth() {
        firebase.auth().signInWithRedirect(provider)
      },
      async callSignOut({ commit }) {
        await firebase.auth().signOut()
        commit('setCredential', { user: null })
      },
      loadStart({ commit }) {
        commit('setIsLoaded', false)
      },
      loadComplete({ commit }) {
        commit('setIsLoaded', true)
      }
    }
  })
}

export default createStore
